<?php

class Hewan{
	public $nama,
		   $darah = 50,
		   $jumlahKaki,
		   $keahlian;

	public function setNama($nama){
		$this->nama = $nama;
	}

	public function setJumlahKaki($jumlahKaki){
		$this->jumlahKaki = $jumlahKaki;
	}

	public function setKeahlian($keahlian){
		$this->keahlian = $keahlian;
	}

	public function atraksi(){
		$str = "{$this->nama} sedang {$this->keahlian}";

		return $str;
	}
}

class Fight extends Hewan{
	public $attackPower,
		   $defencePower;


	public function setAttackPower($attackPower){
		$this->attackPower = $attackPower;
	}

	public function setDefencePower($defencePower){
		$this->defencePower = $defencePower;
	}

	public function serang($namahewan1, $namahewan2){
		$str = "{$namahewan1} sedang menyerang {$namahewan2}";

		return $str;
	}

	public function diserang($namahewan1, $defencePower1, $namahewan2, $attackPower2){
		$darahsekarang = $this->darah - $attackPower2 / $defencePower1;

		$str = "{$namahewan1} sedang diserang {$namahewan2}, darahnya sekarang {$darahsekarang}";

		return $str;
	}
}

class Elang extends Fight{
	public function getInfoHewan(){
		echo "Nama          : {$this->nama}"; echo "<br>";
		echo "Jumlah Kaki   : {$this->jumlahKaki}"; echo "<br>";
		echo "Keahlian      : {$this->keahlian}"; echo "<br>";
		echo "Attack Power  : {$this->attackPower}"; echo "<br>";
		echo "Defence Power : {$this->defencePower}"; echo "<br>";
	}

}

class Harimau extends Fight{
		public function getInfoHewan(){
		echo "Nama          : {$this->nama}"; echo "<br>";
		echo "Jumlah Kaki   : {$this->jumlahKaki}"; echo "<br>";
		echo "Keahlian      : {$this->keahlian}"; echo "<br>";
		echo "Attack Power  : {$this->attackPower}"; echo "<br>";
		echo "Defence Power : {$this->defencePower}"; echo "<br>";
	}
}

$intansiasiElang = new Elang();
$intansiasiElang->setNama("elang_3");
$intansiasiElang->setJumlahKaki(2);
$intansiasiElang->setKeahlian("terbang tinggi");
$intansiasiElang->setAttackPower(10);
$intansiasiElang->setDefencePower(5);

$intansiasiHarimau = new Harimau();
$intansiasiHarimau->setNama("harimau_1");
$intansiasiHarimau->setJumlahKaki(4);
$intansiasiHarimau->setKeahlian("lari cepat");
$intansiasiHarimau->setAttackPower(7);
$intansiasiHarimau->setDefencePower(8);

echo $intansiasiElang->getInfoHewan();
echo "<br>";
echo $intansiasiElang->atraksi();
echo "<br>";
echo $intansiasiElang->serang($intansiasiElang->nama, $intansiasiHarimau->nama);
echo "<br>";
echo $intansiasiElang->diserang($intansiasiElang->nama,
								$intansiasiElang->defencePower,
							    $intansiasiHarimau->nama,
								$intansiasiHarimau->attackPower);

echo "<hr>";

echo $intansiasiHarimau->getInfoHewan();
echo "<br>";
echo $intansiasiHarimau->atraksi();
echo "<br>";
echo $intansiasiElang->serang($intansiasiHarimau->nama, $intansiasiElang->nama);
echo "<br>";
echo $intansiasiElang->diserang($intansiasiHarimau->nama,
								$intansiasiHarimau->defencePower,
								$intansiasiElang->nama,
								$intansiasiElang->attackPower);